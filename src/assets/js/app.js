; (function () {
    'use strict';

    var DRAW_DOT = 'DOT';
    var DRAW_LINE = 'LINE';
    var DRAW_ANGLE = 'ANGLE';
    var CREATE_MODE = 'CREATE';
    var EDIT_MODE = 'EDIT';

    var CANVAS = document.getElementById('image-canvas');
    var CANVAS_CONTEXT = CANVAS.getContext('2d');
    var LINE_WIDTH = 1;

    var CAN_DRAW = false;
    var DRAW_TYPE = null;
    var DRAW_MODE = null;

    var IMAGE_LOADER_WRAPPER = document.getElementById('image-loader-wrapper');
    var IMAGE_LOADER = document.getElementById('image-loader');

    var DOT_CREATOR_ID = 'dot-creator';
    var DOT_CREATOR = document.getElementById(DOT_CREATOR_ID);

    var LINE_CREATOR_ID = 'line-creator';
    var LINE_CREATOR = document.getElementById(LINE_CREATOR_ID);

    var ANGLE_CREATOR_ID = 'angle-creator';
    var ANGLE_CREATOR = document.getElementById(ANGLE_CREATOR_ID);

    var OBJECTS_HOLDER = document.getElementById('objects-holder');

    var COLOR = null;
    var IMAGE = null;
    var OBJECTS = {};
    var CLICK_COUNT = 0;
    var COORDINATES = [];
    var ACTIVE_OBJECT_UUID = null;
    var ACTIVE_OBJECT = null;

    var REFERENCE_LENGTH_INPUT = document.getElementById('reference-length');
    var REFERENCE_LENGTH = 1;

    var REAL_LENGTH_INPUT = document.getElementById('real-length');
    var REAL_LENGTH = 1;

    var SCALE_FACTOR_INPUT = document.getElementById('scale-factor');
    var SCALE_FACTOR = 1;

    var ZOOM_IN_INPUT = document.getElementById('zoom-in');
    var ZOOM_OUT_INPUT = document.getElementById('zoom-out');
    var ZOOM_LEVEL_INPUT = document.getElementById('zoom-level');
    var ZOOM_LEVEL = 1;

    var ROTATION_PLUS_INPUT = document.getElementById('rotation-plus');
    var ROTATION_MINUS_INPUT = document.getElementById('rotation-minus');
    var ROTATION_ANGLE_INPUT = document.getElementById('rotation-angle');
    var ROTATION_ANGLE = 0;

    var RESIZE_ZOOM_FACTOR = 1;
    var ORIGINAL_CANVAS_HEIGHT = null;
    var ORIGINAL_CANVAS_WIDTH = null;

    function computeDistance (coordinates) {
        var x0 = coordinates[0].x;
        var y0 = coordinates[0].y;

        var x1 = coordinates[1].x;
        var y1 = coordinates[1].y;

        return Math.sqrt(Math.pow(x1 - x0, 2) + Math.pow(y1 - y0, 2)) / SCALE_FACTOR;
    }

    function getRotationMatrix (angle) {
        var rad = angle * Math.PI / 180;
        return [
            [Math.cos(rad), -Math.sin(rad)],
            [Math.sin(rad), Math.cos(rad)],
        ];
    }

    function computeAngle (coordinates) {
        var x0 = coordinates[0].x;
        var y0 = coordinates[0].y;

        var x1 = coordinates[1].x;
        var y1 = coordinates[1].y;

        var x2 = coordinates[2].x;
        var y2 = coordinates[2].y;

        var x3 = coordinates[3].x;
        var y3 = coordinates[3].y;

        var vx1 = x1 - x0;
        var vy1 = y1 - y0;

        var vx2 = x3 - x2;
        var vy2 = y3 - y2;

        var dot = vx1 * vx2 + vy1 * vy2;

        var len1 = Math.sqrt(vx1 * vx1 + vy1 * vy1);
        var len2 = Math.sqrt(vx2 * vx2 + vy2 * vy2);

        var angle = Math.acos(dot / (len1 * len2));

        return Math.min(angle, Math.PI - angle) * 180 / Math.PI;
    }

    function getMousePosition (canvas, evt) {
        var rect = canvas.getBoundingClientRect();

        // console.log(evt.clientX, evt.clientY);

        var x = (evt.clientX - rect.left) / ZOOM_LEVEL;
        var y = (evt.clientY - rect.top) / ZOOM_LEVEL;

        // var rotationMatrix = getRotationMatrix(ROTATION_ANGLE);

        // var rotatedX = rotationMatrix[0][0] * (x + CANVAS.width / 2) + rotationMatrix[0][1] * (y + CANVAS.height / 2) - CANVAS.width / 2;
        // var rotatedY = rotationMatrix[1][0] * (x + CANVAS.width / 2) + rotationMatrix[1][1] * (y + CANVAS.height / 2) - CANVAS.height / 2;

        return {
            x: x, // rotatedX,
            y: y, // rotatedY
        };
    }

    function getRandomBrightColor () {
        return {
            h: 360 * Math.random(),
            s: 100,
            l: 50
        };
    }

    function hsl2rgb (hsl) {
        var h = hsl.h;
        var s = hsl.s;
        var l = hsl.l;

        s /= 100;
        l /= 100;

        var a = s * Math.min(l, 1 - l);

        function k (n) {
            return (n + h / 30) % 12;
        }

        function f (n) {
            return l - a * Math.max(-1, Math.min(k(n) - 3, Math.min(9 - k(n), 1)));
        }

        return {
            r: 255 * f(0),
            g: 255 * f(8),
            b: 255 * f(4),
        };
    };


    function rgb2hex (rgb) {
        return '#' + [rgb.r, rgb.g, rgb.b].map(function (x) {
            var hex = Math.round(x).toString(16);
            return hex.length === 1 ? '0' + hex : hex;
        }).join('');
    }

    function getUUID () {
        var dt = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (dt + Math.random() * 16) % 16 | 0;
            dt = Math.floor(dt / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    }

    function drawImage (event) {
        IMAGE.src = URL.createObjectURL(event.target.files[0]);
    }

    function drawDot (coordinates, color) {
        var rgbColor = hsl2rgb(color);
        CANVAS_CONTEXT.beginPath();
        CANVAS_CONTEXT.fillStyle = 'rgba(' + rgbColor.r + ', ' + rgbColor.g + ', ' + rgbColor.b + ', 0.5)';
        CANVAS_CONTEXT.arc(coordinates.x * ZOOM_LEVEL, coordinates.y * ZOOM_LEVEL, 10, 0, 2 * Math.PI);
        CANVAS_CONTEXT.fill();
        CANVAS_CONTEXT.lineWidth = LINE_WIDTH;
        CANVAS_CONTEXT.strokeStyle = 'rgba(' + rgbColor.r + ', ' + rgbColor.g + ', ' + rgbColor.b + ', 1)';
        CANVAS_CONTEXT.stroke();
    }

    function drawLine (coordinates, color) {
        CANVAS_CONTEXT.lineWidth = LINE_WIDTH;
        CANVAS_CONTEXT.strokeStyle = color;
        CANVAS_CONTEXT.beginPath();
        CANVAS_CONTEXT.moveTo(coordinates[0].x * ZOOM_LEVEL, coordinates[0].y * ZOOM_LEVEL);
        CANVAS_CONTEXT.lineTo(coordinates[1].x * ZOOM_LEVEL, coordinates[1].y * ZOOM_LEVEL);
        CANVAS_CONTEXT.stroke();
    }

    function initImage () {
        IMAGE = new Image;
        IMAGE.onload = function () {
            refreshCanvas();
        }
    }

    function readImageFromURL () {
        var url = new URL(window.location.href)
        var image = url.searchParams.get('image');
        var reader = new FileReader();
        reader.addEventListener('load', function () {
            var data = reader.result;
            IMAGE.src = data;
            IMAGE_LOADER_WRAPPER.style.display = 'none';
        });

        if (image) {
            fetch(image).then(function (response) {
                return response.blob();
            }).then(function (blob) {
                reader.readAsDataURL(blob);
            });
        }
    }

    function refreshCanvas () {
        CANVAS.width = CANVAS.parentElement.clientWidth * ZOOM_LEVEL;
        CANVAS.height = CANVAS.parentElement.clientHeight * ZOOM_LEVEL;
        CANVAS_CONTEXT.clearRect(0, 0, CANVAS.width, CANVAS.height);

        if (IMAGE) {
            var image_aspect_ratio = IMAGE.width / IMAGE.height;
            var canvas_aspect_ratio = CANVAS.width / CANVAS.height;

            if (image_aspect_ratio > canvas_aspect_ratio) {
                var new_width = CANVAS.width;
                var new_height = new_width / image_aspect_ratio;
            } else {
                var new_height = CANVAS.height;
                var new_width = new_height * image_aspect_ratio;
            }

            CANVAS_CONTEXT.drawImage(IMAGE, 0, 0, new_width, new_height);

            /*
            if (ORIGINAL_CANVAS_HEIGHT === null || ORIGINAL_CANVAS_WIDTH === null) {
                ORIGINAL_CANVAS_HEIGHT = new_height;
                ORIGINAL_CANVAS_WIDTH = new_width;
            } else {
                RESIZE_ZOOM_FACTOR = Math.min(new_width / ORIGINAL_CANVAS_WIDTH, new_height / ORIGINAL_CANVAS_HEIGHT);

                if (RESIZE_ZOOM_FACTOR !== 1) {
                    var objectIds = Object.keys(OBJECTS);
                    for (var i = 0; i < objectIds.length; i++) {
                        var objectId = objectIds[i];
                        var obj = OBJECTS[objectId];

                        for (var j = 0; j < obj.coordinates.length; j++) {
                            obj.coordinates[j].x *= RESIZE_ZOOM_FACTOR;
                            obj.coordinates[j].y *= RESIZE_ZOOM_FACTOR;
                        }

                        OBJECTS[objectId] = obj;
                    }
                }

                ORIGINAL_CANVAS_HEIGHT = new_height;
                ORIGINAL_CANVAS_WIDTH = new_width;
                RESIZE_ZOOM_FACTOR = 1;
            }
            */
        }

        var objectIds = Object.keys(OBJECTS);
        for (var i = 0; i < objectIds.length; i++) {
            var obj = OBJECTS[objectIds[i]];

            for (var j = 0; j < obj.coordinates.length; j++) {
                drawDot(obj.coordinates[j], obj.color);

                if (j % 2 === 1) {
                    drawLine([obj.coordinates[j - 1], obj.coordinates[j]], obj.color);
                }
            }
        }
    }

    function setDrawType (drawType) {
        DRAW_TYPE = drawType;
    }

    function setDrawMode (drawMode) {
        DRAW_MODE = drawMode;
    }

    function toggleCanvasDrawing () {
        CAN_DRAW = !CAN_DRAW;
    }

    function enableActions () {
        var actions = document.querySelectorAll('.x-ray__action-btn');

        for (var i = 0; i < actions.length; i++) {
            actions[i].disabled = false;
        }

    }

    function disableActions () {
        var actions = document.querySelectorAll('.x-ray__action-btn');

        for (var i = 0; i < actions.length; i++) {
            actions[i].disabled = true;
        }
    }

    function disableCreator (element) {
        element.disabled = true;
        element.classList.add('disabled');
    }

    function enableCreator (element) {
        element.disabled = false;
        element.classList.remove('disabled');
    }

    function activateCreator (element) {
        element.textContent = 'Finalizar';
    }

    function deactivateCreator (element) {
        var icon = document.createElement('i');
        icon.classList.add('material-symbols-outlined');
        icon.classList.add('mr-2');
        icon.textContent = 'add';

        var label = document.createElement('span');

        if (element.id == DOT_CREATOR_ID) {
            label.textContent = 'Añadir Punto';
        } else if (element.id == LINE_CREATOR_ID) {
            label.textContent = 'Añadir Línea';
        } else if (element.id == ANGLE_CREATOR_ID) {
            label.textContent = 'Añadir Ángulo';
        }

        element.innerHTML = '';
        element.appendChild(icon);
        element.appendChild(label);
    }

    function clickOnDotCreator () {
        toggleCanvasDrawing();

        if (CAN_DRAW) {
            activateCreator(DOT_CREATOR);
            disableCreator(LINE_CREATOR);
            disableCreator(ANGLE_CREATOR);

            setDrawMode(CREATE_MODE);
            setDrawType(DRAW_DOT);
            disableActions();
        } else {
            deactivateCreator(DOT_CREATOR);
            enableCreator(LINE_CREATOR);
            enableCreator(ANGLE_CREATOR);

            setDrawMode(null);
            setDrawType(null);

            enableActions();

            CLICK_COUNT = 0;
            CAN_DRAW = false;
            COORDINATES = [];
            COLOR = null;

            refreshCanvas();
        }
    }

    function clickOnLineCreator () {
        toggleCanvasDrawing();

        if (CAN_DRAW) {
            disableCreator(DOT_CREATOR);
            activateCreator(LINE_CREATOR);
            disableCreator(ANGLE_CREATOR);

            setDrawMode(CREATE_MODE);
            setDrawType(DRAW_LINE);
            disableActions();
        } else {
            enableCreator(DOT_CREATOR);
            deactivateCreator(LINE_CREATOR);
            enableCreator(ANGLE_CREATOR);

            setDrawMode(null);
            setDrawType(null);

            enableActions();

            CLICK_COUNT = 0;
            CAN_DRAW = false;
            COORDINATES = [];
            COLOR = null;

            refreshCanvas();
        }
    }

    function clickOnAngleCreator () {
        toggleCanvasDrawing();

        if (CAN_DRAW) {
            disableCreator(DOT_CREATOR);
            disableCreator(LINE_CREATOR);
            activateCreator(ANGLE_CREATOR);

            setDrawMode(CREATE_MODE);
            setDrawType(DRAW_ANGLE);
            disableActions();
        } else {
            enableCreator(DOT_CREATOR);
            enableCreator(LINE_CREATOR);
            deactivateCreator(ANGLE_CREATOR);

            setDrawMode(null);
            setDrawType(null);

            enableActions();

            CLICK_COUNT = 0;
            CAN_DRAW = false;
            COORDINATES = [];
            COLOR = null;

            refreshCanvas();
        }
    }

    function removeObject (event) {
        var item = event.target.closest(".collection-item")
        var uuid = item.getAttribute('data-uuid');

        delete OBJECTS[uuid];

        refreshCanvas();
        OBJECTS_HOLDER.removeChild(item);
    }

    function editObject (event) {
        setDrawMode(EDIT_MODE);
        disableActions();
        toggleCanvasDrawing();

        var item = event.target.closest(".collection-item")

        var uuid = item.getAttribute('data-uuid');
        var drawType = item.getAttribute('data-draw-type');

        ACTIVE_OBJECT = OBJECTS[uuid];
        delete OBJECTS[uuid];

        DRAW_TYPE = drawType
        COLOR = ACTIVE_OBJECT.color;
        COORDINATES = ACTIVE_OBJECT.coordinates;
        ACTIVE_OBJECT = ACTIVE_OBJECT;
        ACTIVE_OBJECT_UUID = uuid;
    }

    function createObject (uuid) {
        var object = OBJECTS[uuid];
        var avatarText = null;
        var objectText = null;
        var objectMeasure = null;
        var objectMeasureUnits = null;
        var objectComplementaryMeasure = null;
        var objectComplementaryMeasureUnits = null;

        if (object.drawType === DRAW_DOT) {
            avatarText = 'remove';
            objectText = 'Punto';
            objectMeasure = "";
            objectMeasureUnits = "";
            objectComplementaryMeasure = "";
            objectComplementaryMeasureUnits = "";
        } else if (object.drawType === DRAW_LINE) {
            avatarText = 'remove';
            objectText = 'Línea';
            objectMeasure = computeDistance(object.coordinates).toFixed(2);
            objectMeasureUnits = ' cm';
            objectComplementaryMeasure = computeAngle(object.coordinates.concat([{ x: 0, y: 0 }, { x: 1, y: 0 }])).toFixed(2);
            objectComplementaryMeasureUnits = 'º';
        } else if (object.drawType === DRAW_ANGLE) {
            avatarText = 'arrow_back_ios_new';
            objectText = 'Ángulo';
            objectMeasure = computeAngle(object.coordinates).toFixed(2);
            objectMeasureUnits = 'º';
            objectComplementaryMeasure = (180 - objectMeasure).toFixed(2);
            objectComplementaryMeasureUnits = 'º';
        }

        var old_item = OBJECTS_HOLDER.querySelector("#item-" + uuid);

        var item = document.createElement('li');
        item.classList.add('collection-item');
        item.classList.add('avatar');
        item.classList.add('x-ray-reader__item');
        item.setAttribute('id', "item-" + uuid);
        item.setAttribute('data-uuid', uuid);
        item.setAttribute('data-draw-type', object.drawType);

        var avatar = document.createElement('i');
        avatar.classList.add('material-symbols-outlined');
        avatar.classList.add('circle');
        avatar.style.backgroundColor = "hsl(" + object.color.h + ", " + object.color.s + "%, " + object.color.l + "%)";
        avatar.textContent = avatarText;

        var title = document.createElement('span');
        title.classList.add('title');
        title.textContent = objectText;

        var measure = document.createElement('p');
        measure.classList.add('measure');
        measure.textContent = objectMeasure + objectMeasureUnits;

        var complmentaryMeasure = document.createElement('p');
        complmentaryMeasure.classList.add('complmentary-measure');
        complmentaryMeasure.textContent = objectComplementaryMeasure + objectComplementaryMeasureUnits;

        var actions = document.createElement('div');
        actions.classList.add('actions');
        actions.classList.add('text-right');

        var editAction = document.createElement('button');
        editAction.classList.add('x-ray__action-btn');
        editAction.classList.add('btn');
        editAction.classList.add('btn-flat');
        editAction.classList.add('btn-small');
        editAction.classList.add('waves-effect');
        editAction.classList.add('waves-dark');
        editAction.classList.add('white');
        editAction.classList.add('green-text');
        editAction.classList.add('pointer');
        editAction.classList.add('block');
        editAction.setAttribute('href', '#');

        var editIcon = document.createElement('i');
        editIcon.classList.add('material-symbols-outlined');
        editIcon.textContent = "edit";

        var removeAction = document.createElement('button');
        removeAction.classList.add('x-ray__action-btn');
        removeAction.classList.add('btn');
        removeAction.classList.add('btn-flat');
        removeAction.classList.add('btn-small');
        removeAction.classList.add('waves-effect');
        removeAction.classList.add('waves-dark');
        removeAction.classList.add('white');
        removeAction.classList.add('red-text');
        removeAction.classList.add('pointer');
        removeAction.classList.add('block');
        removeAction.classList.add('mt-2');
        removeAction.setAttribute('href', '#');

        var removeIcon = document.createElement('i');
        removeIcon.classList.add('material-symbols-outlined');
        removeIcon.textContent = "delete";

        editAction.appendChild(editIcon);
        editAction.addEventListener('click', editObject);

        removeAction.appendChild(removeIcon);
        removeAction.addEventListener('click', removeObject);

        actions.appendChild(editAction);
        actions.appendChild(removeAction);

        item.appendChild(avatar);
        item.appendChild(title);
        item.appendChild(measure);
        item.appendChild(complmentaryMeasure);
        item.appendChild(actions);

        if (old_item) {
            OBJECTS_HOLDER.replaceChild(item, old_item);
        } else {
            OBJECTS_HOLDER.appendChild(item);
        }
    }

    function refreshObjectList (event) {
        REFERENCE_LENGTH = parseFloat(REFERENCE_LENGTH_INPUT.value);
        REAL_LENGTH = parseFloat(REAL_LENGTH_INPUT.value);
        SCALE_FACTOR = REFERENCE_LENGTH / REAL_LENGTH;
        SCALE_FACTOR_INPUT.value = SCALE_FACTOR;

        OBJECTS_HOLDER.innerHTML = '';

        for (var uuid in OBJECTS) {
            createObject(uuid);
        }
    }

    function handleCreate (event) {
        console.log(DRAW_TYPE, CLICK_COUNT, COLOR);

        if (DRAW_TYPE === DRAW_DOT) {
            var maxClickCount = 1;
        } else if (DRAW_TYPE === DRAW_LINE) {
            var maxClickCount = 2;
        } else if (DRAW_TYPE === DRAW_ANGLE) {
            var maxClickCount = 4;
        }

        if (CLICK_COUNT < maxClickCount) {
            if (COLOR === null) {
                COLOR = getRandomBrightColor();
            }

            var pos = getMousePosition(CANVAS, event);
            COORDINATES.push(pos);

            drawDot(COORDINATES[CLICK_COUNT], COLOR);

            if (CLICK_COUNT % 2 == 1) {
                drawLine(COORDINATES.slice(CLICK_COUNT - 1, CLICK_COUNT + 1), COLOR);
            }

            CLICK_COUNT++;
        }

        if (CLICK_COUNT === maxClickCount) {
            var uuid = getUUID();

            if (ACTIVE_OBJECT_UUID !== null) {
                uuid = ACTIVE_OBJECT_UUID;
            }

            OBJECTS[uuid] = {
                uuid: uuid,
                color: COLOR,
                coordinates: COORDINATES,
                drawType: DRAW_TYPE
            };

            createObject(uuid);

            CLICK_COUNT = 0;
            CAN_DRAW = false;
            COORDINATES = [];
            COLOR = null;

            ACTIVE_OBJECT_UUID = null;
            ACTIVE_OBJECT = null;

            setDrawType(null);

            enableCreator(DOT_CREATOR);
            deactivateCreator(DOT_CREATOR);

            enableCreator(LINE_CREATOR);
            deactivateCreator(LINE_CREATOR);

            enableCreator(ANGLE_CREATOR);
            deactivateCreator(ANGLE_CREATOR);

            enableActions();
            refreshCanvas();
        }
    }

    function handleEdit (event) {
        var pos = getMousePosition(CANVAS, event);

        for (var i = 0; i < COORDINATES.length; i++) {
            var coordinate = COORDINATES[i];
            var distance = computeDistance([coordinate, pos]);
            if (distance < 10) {
                if (DRAW_TYPE === DRAW_DOT) {
                    COORDINATES = [];

                    CLICK_COUNT = 0;
                }

                if (DRAW_TYPE === DRAW_LINE) {
                    var p0 = COORDINATES[0];
                    var p1 = COORDINATES[1];

                    if (i == 0) {
                        COORDINATES = [p1];
                    }

                    if (i == 1) {
                        COORDINATES = [p0];
                    }

                    CLICK_COUNT = 1;
                }

                if (DRAW_TYPE === DRAW_ANGLE) {
                    var p0 = COORDINATES[0];
                    var p1 = COORDINATES[1];
                    var p2 = COORDINATES[2];
                    var p3 = COORDINATES[3];

                    if (i == 0) {
                        COORDINATES = [p2, p3, p1];
                    }

                    if (i == 1) {
                        COORDINATES = [p2, p3, p0];
                    }

                    if (i == 2) {
                        COORDINATES = [p0, p1, p3];
                    }

                    if (i == 3) {
                        COORDINATES = [p0, p1, p2];
                    }

                    CLICK_COUNT = 3;
                }

                DRAW_MODE = CREATE_MODE;

                break;
            }
        }
    }

    function handleCanvasClick (event) {
        if (CAN_DRAW) {
            if (DRAW_MODE === CREATE_MODE) {
                handleCreate(event);
            }

            if (DRAW_MODE === EDIT_MODE) {
                handleEdit(event);
            }
        }
    }

    function handleMoseOverOnCanvas (event) {
        if (CAN_DRAW) {
            refreshCanvas();

            if (COLOR === null) {
                COLOR = getRandomBrightColor();
            }

            var pos = getMousePosition(CANVAS, event);
            drawDot(pos, COLOR);

            if (COORDINATES.length > 0 && CLICK_COUNT % 2 == 1) {
                var coordinates = COORDINATES.slice(-1)
                coordinates.push(pos);
                drawLine(coordinates, COLOR);
            }

            for (var i = 0; i < COORDINATES.length; i++) {
                drawDot(COORDINATES[i], COLOR);

                if (i % 2 == 1) {
                    drawLine(COORDINATES.slice(i - 1, i + 1), COLOR);
                }
            }
        }
    }

    function handleZoomIn () {
        if (ZOOM_LEVEL < 4) {
            ZOOM_LEVEL++;
            ZOOM_LEVEL_INPUT.innerText = 'x' + ZOOM_LEVEL.toString();
            refreshCanvas();
        }
    }

    function handleZoomOut () {
        if (ZOOM_LEVEL > 1) {
            ZOOM_LEVEL--;
            ZOOM_LEVEL_INPUT.innerText = 'x' + ZOOM_LEVEL.toString();
            refreshCanvas();
        }
    }

    function handleRotationPlus () {
        if (ROTATION_ANGLE < 360) {
            ROTATION_ANGLE += 5;
            if (ROTATION_ANGLE >= 360) {
                ROTATION_ANGLE = 0;
            }
            ROTATION_ANGLE_INPUT.value = ROTATION_ANGLE.toString();
            refreshCanvas();
            rotateCanvas(ROTATION_ANGLE);
        }
    }

    function handleRotationMinus () {
        if (ROTATION_ANGLE > 0) {
            ROTATION_ANGLE -= 5;
            if (ROTATION_ANGLE < 0) {
                ROTATION_ANGLE = 0;
            }
            ROTATION_ANGLE_INPUT.value = ROTATION_ANGLE.toString();
            refreshCanvas();
            rotateCanvas(ROTATION_ANGLE);
        }
    }

    function handleRotationInputChange (event) {
        var value = parseInt(event.target.value);
        if (value < 0 || value >= 360) {
            value = 0;
        }
        ROTATION_ANGLE = value;
        ROTATION_ANGLE_INPUT.value = ROTATION_ANGLE.toString();
        refreshCanvas();
        rotateCanvas(ROTATION_ANGLE);
        // console.log(CANVAS.getBoundingClientRect());
    }

    function rotateCanvas (angle) {
        CANVAS.style.transform = 'rotate(' + angle + 'deg)';
    }

    document.addEventListener('DOMContentLoaded', function () {
        M.Tabs.init(document.getElementById("menu-tabs"), { swipeable: true });
        // M.AutoInit();
    });

    initImage();
    readImageFromURL();
    ZOOM_IN_INPUT.addEventListener('click', handleZoomIn);
    ZOOM_OUT_INPUT.addEventListener('click', handleZoomOut);

    ROTATION_PLUS_INPUT.addEventListener('click', handleRotationPlus);
    ROTATION_MINUS_INPUT.addEventListener('click', handleRotationMinus);
    ROTATION_ANGLE_INPUT.addEventListener('change', handleRotationInputChange);

    REFERENCE_LENGTH_INPUT.value = SCALE_FACTOR;
    REFERENCE_LENGTH_INPUT.addEventListener('change', refreshObjectList);

    REAL_LENGTH_INPUT.value = SCALE_FACTOR;
    REAL_LENGTH_INPUT.addEventListener('change', refreshObjectList);

    SCALE_FACTOR_INPUT.value = SCALE_FACTOR;
    SCALE_FACTOR_INPUT.addEventListener('change', refreshObjectList);

    IMAGE_LOADER.addEventListener('change', drawImage);

    DOT_CREATOR.addEventListener('click', clickOnDotCreator);
    LINE_CREATOR.addEventListener('click', clickOnLineCreator);
    ANGLE_CREATOR.addEventListener('click', clickOnAngleCreator);

    CANVAS.addEventListener('click', handleCanvasClick);
    CANVAS.addEventListener('mousemove', handleMoseOverOnCanvas);
    window.addEventListener('resize', refreshCanvas);
})();
